<?php

namespace App\DataFixtures;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {

        $numPhoto=array(6,7,9,10,12,13,15,16);
        $nom=array('Wampas','Brain Damage','JP Manova','Sax Machine','The Stranglers','The Bad Plus','Maalouf','Fat Freddy\'s Drop');
        $prenom=array('Didier','','','','','','Ibrahim','');
        $artistes=array();
        $nb=count($numPhoto);
        for($i=0;$i<$nb;$i++) {
            $artiste = new Artiste(
                $nom[$i] . "",
                $prenom[$i] . "",
                $numPhoto[$i] . ".jpg",
                $nom[$i] . " Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
            );
            $artistes[]=$artiste;
            $manager->persist($artiste);
        }

        for($i=0;$i<$nb;$i++){
            $concert=new Concert(new \DateTime(mt_rand(-60,+60).' day'),"Super Concert",mt_rand(10,50));
            $concert->setArtiste($artistes[$i]);
            $manager->persist($concert);
        }

        $user1 = new User();
        $user1->setNom("Thomy");
        $user1->setPrenom("Loan");
        $user1->setUsername("Loan01");
        $user1->setEmail("loan.thomy@gmail.com");
        $user1->setDateNaissance(new \DateTime("30-11-2003"));
        $user1->setRoles(['ROLE_ADMIN']);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user1,
            'loan'
        );
        $user1->setPassword($hashedPassword);

        $manager->persist($user1);

        $user2 = new User();
        $user2->setNom("Tallon");
        $user2->setPrenom("Paul");
        $user2->setUsername("situto");
        $user2->setEmail("paul.tallon@gmail.com");
        $user2->setDateNaissance(new \DateTime("30-11-2003"));
        $user2->setRoles(['ROLE_USER']);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user2,
            'paul'
        );
        $user2->setPassword($hashedPassword);

        $manager->persist($user2);

        $manager->flush();
    }
}
